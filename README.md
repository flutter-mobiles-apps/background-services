# Flutter Isolate

Tests are performed with a flutter package to perform actions in the background, using Timers, periodically we can perform tests to use pause, play, and delete, the compute elements that practically create an Isolate and execute the actions in the background for, maneter both in Android and IOS, the functionalities.

## Paquetes

- [Path_provider](https://pub.dev/packages/path_provider/install)
- [Flutter_isolate](https://pub.dev/packages/flutter_isolate)

## References


- [Flutter Isolate API](https://api.flutter.dev/flutter/dart-isolate/Isolate-class.html)
- [Explanation about Isolates and background services](https://www.youtube.com/watch?v=cV0pByqNV6A)
